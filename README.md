# A simple physic engine

## Introduction

The current project is a very simple 2D physic engine which can simulate the behaviour of several sherical bodies (gravity and collisions management). The bodies attract each other and gain heat and change color when they collide. You can load a pre-made configuration or make and save your own.

![simulation](images/simulation.png)

/!\ some of the functionalities described below are still a work in progress /!\

## Shortcuts

The application can be controlled entirely with the keyboard.

###### Navigation

* Z : zoom in
* S : zoom out
* Arrow keys : move the camera
* C : reinitialize the camera (position and zoom)
* P : pause / unpause the simulation
* R : decrease simulation's speed
* T : increase simulation's speed

###### Editing

* Ctrl + E : pause the simulation and got to edit mode
* Ctrl + S : save the configuration and return to simulation. This conf will be used at the next start-up of the program
* Escape : (in edit mode) return to simulation without saving the conf

###### Others

* Escape : (in simulation mode) quit the program
* Ctrl + L : open the configuration selection menu

## Installation

If you use linux, you can follow the following steps to compile the files

1. Be sure to have a c++ compiler, cmake, and the SFML2 library installed
2. Download and go to the root of the project
3. Run the following commands :
```
cmake .
make
./simulation
```

