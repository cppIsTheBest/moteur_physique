#include <cmath>
#include <sstream>
#include <iostream>
#include <fstream>
#include "Simulation.h"
#include "Structures.h"
#include "Parameters.h"
#include "constantes.h"
#include "BodyCreator.h"

using namespace sf;
#define arrondi(a) int(a + 0.5)
using namespace std;

sf::Vector2i windowPos2SimulPos(sf::Vector2i const& windowPos,
                                Couple const& decalageMap);

Simulation::Simulation()
{
    std::cout << "simul creee" << std::endl;
    start_time = milliseconds(0);
    nb_iterations = 0; 

    if( Parameters::params["MODE_INIT"] == 0 )
        init_corps();
    else if ( Parameters::params["MODE_INIT"] == 1 )
        init_grille_corps();
    else
        init_nuage_corps();

    tOb = objets.size();
    for (unsigned int i=0; i<tOb; i++)
        ind_corps_traites.push_back(0);
    initTpsCol();

    BodyCreator::init();
}



Simulation::~Simulation() {
    /*for (unsigned int i(0); i<objets.size(); i++) {
        delete objets[i];
    }
    objets.clear();*/
}



void Simulation::creer_img()
{
    for(unsigned int i(0);i<objets.size();i++)
        objets[i]->create_img(Parameters::params["ECHELLE"]);
}



void Simulation::init_corps()
{
    string filePath = "Data/currentPositionningParam.txt";
    ifstream fichier(filePath);

    if(fichier)
    {
        double x, y, vx, vy, r, id(0);

        string motCourant;
        fichier >> motCourant;

        while( motCourant != "FIN_PARAMETRAGE" )
        {
            while( motCourant != "CORPS" )
                fichier >> motCourant;

            fichier >> x;
            fichier >> y;
            fichier >> vx;
            fichier >> vy;
            fichier >> r;
            objets.push_back( new Corps(x, y, vx, vy, r, id) );

            id ++;
            fichier >> motCourant;
        }
    }
    else {
        cout << endl << "Erreur : Impossible d'ouvrir le fichier ";
        cout << filePath << " en lecture" << endl;
    }
}



void Simulation::init_grille_corps()
{
    double nbCorps = Parameters::paramsGrid["BODIES_COUNT"];
    double f = Parameters::paramsGrid["RADIUS"];
    double r0 = Parameters::paramsGrid["GAP"];

    double d = f*r0;
    unsigned int n(arrondi(sqrt(nbCorps)));
    double tGrille(n*r0+(n-1)*d);

    for(unsigned int i(0);i<n;i++)
        for(unsigned int j(0);j<n;j++)
            objets.push_back(
                new Corps(i*d-tGrille/2, j*d-tGrille/2, 0, 0, r0, i*n + j) );
}



double frand(double a, double b);
Etat suivant(Etat etat);
double calculAngleVecteur(double dx, double dy);

void Simulation::init_nuage_corps()
{
    double nbCorps = Parameters::paramsCloud["NOMBRE_CORPS"];
    double rayon_base = Parameters::paramsCloud["RAYON_BASE"];
    double rayon_variation = Parameters::paramsCloud["RAYON_VARIATION"];
    double vitesse_base = Parameters::paramsCloud["VITESSE_BASE"];
    double vitesse_variation = Parameters::paramsCloud["VITESSE_VARIATION"];
    bool v_spiralees = Parameters::paramsCloud["V_SPIRALEES"] != 0;
    double eloignement_base = Parameters::paramsCloud["ELOIGNEMENT_BASE"];
    double eloignement_variation =
                        Parameters::paramsCloud["ELOIGNEMENT_VARIATION"];

    double m_blackhole = 20;

    //initialisation du nuage
    Etat etat = D;

    double r, vx, vy, norme_v, alpha, eloignement;
    double x(0), y(0);
    double distance;

    unsigned int nbTour(0), k(0);
    for(unsigned int i(0);i<nbCorps-v_spiralees;i++)
    {
        r = frand(rayon_base-rayon_variation, rayon_base+rayon_variation);
        eloignement = frand(eloignement_base-eloignement_variation,
                            eloignement_base+eloignement_variation);

        switch(etat)
        {
            case D:
                x += eloignement;
                break;
            case BD:
                x += eloignement/sqrt(2);
                y += eloignement/sqrt(2);
                break;
            case B:
                y += eloignement;
                break;
            case BG:
                x -= eloignement/sqrt(2);
                y += eloignement/sqrt(2);
                break;
            case G:
                x -= eloignement;
                break;
            case HG:
                x -= eloignement/sqrt(2);
                y -= eloignement/sqrt(2);
                break;
            case H:
                y -= eloignement;
                break;
            case HD:
                y -= eloignement/sqrt(2);
                x += eloignement/sqrt(2);
                break;
            default:
                break;
        }
        
        //vitesses spiralees ou totalement aleatoires
        if(v_spiralees)
        {   distance = sqrt(pow(x,2)+pow(y,2));
            norme_v = sqrt(Parameters::params["G"]*pow(m_blackhole,2))
                            /sqrt(distance);
            alpha = calculAngleVecteur(x, y);
            vx = cos(alpha+M_PI_2)*norme_v;
            vy = sin(alpha+M_PI_2)*norme_v;
        }
        else
        {   vx = frand(vitesse_base-vitesse_variation,
                       vitesse_base+vitesse_variation);
            if(rand()/(double)RAND_MAX < 0.5)
                vx = -vx;
            vy = frand(vitesse_base-vitesse_variation,
                       vitesse_base+vitesse_variation);
            if(rand()/(double)RAND_MAX < 0.5)
                vy = -vy;

        }

        if(k < nbTour)
            k++;
        else
        {   etat = suivant(etat);
            k = 0;
            if(etat == D)
            {   nbTour ++;
                x = -(double(nbTour)/2)*eloignement;;
                y = -double(nbTour)*(0.5+(1/sqrt(2)))*eloignement;
            }
        }
        objets.push_back( new Corps(x, y, vx, vy, r, i) );
    }
    // si on demande un modele avec des vitesses en spirales, on place un
    // objet tres massif au centre de la "galaxie"
    if (v_spiralees) 
        objets.push_back( new Corps(0, 0, 0, 0, m_blackhole, nbCorps-1) );
}



unsigned int compte_nb_elts(vector< vector< double > > const& tpsChoc,
                            unsigned int taille);
Couple tpsColMin(vector< vector< double > > const& tpsCol, unsigned int tOb);
unsigned int suppression_tps(vector< vector< double > > & tpsCol,
            unsigned int taille, unsigned int nb_elts, Couple const& indices);
void triRapide(vector< unsigned int > & tableau, int debut, int fin);

void Simulation::actualisation()
{
    if(nb_iterations == 0)
        start_time = clock.getElapsedTime();
    
    nb_iterations ++;

    // on rempli la matrice triangulaire superieure contenant tous les temps
    // de collision entre les corps
    computeTpsCol();

    // gestion des collisions qui auront effectivement lieu
    gestionChoc();

    // gestion des corps qui n'ont pas deja etes traites
    gestionCorpsRestants();

    // on actualise l'acceleration subie par tous les objets
    for(unsigned int i(0);i<tOb;i++)
        if (!BodyCreator::newBodyExists || i != objets.size()-1)
            objets[i]->updateAcceleration(objets, Parameters::params["G"]);
}



void Simulation::affiche_performances()
{
    if (Parameters::params["DT_DEBLOQUE"] == 1)
        cout << "dt debloque : oui" << endl;
    else
        cout << "dt debloque : non" << endl;
    float tps_ecoule  = (clock.getElapsedTime()-start_time).asSeconds();
    cout << "temps ecoulé : " << tps_ecoule << endl;
    float it_s_reel = (float)(nb_iterations)/tps_ecoule;
    float it_s_the = 1/(float)(Parameters::params["DT"]);
    cout << "nombres d'iterations/s executees : " << it_s_reel << endl;
    cout << "nombre d'it/s theorique (d'apres le dt du fichier parametres.txt)";
    cout << " : " << it_s_the << endl;
    cout << "facteur de lenteur (ex 2 = 2x plus lent que voulu) : ";
    cout << it_s_the/it_s_reel << endl;
}



void Simulation::createNewBody(Couple const& decalageMap,
                                RenderWindow & window) {
    Vector2i pos = windowPos2SimulPos(Mouse::getPosition(window), decalageMap);
    objets.push_back(new Corps(pos.x, pos.y, 0, 0, 15, objets.size()));
    tOb = objets.size();
    initTpsCol();
}



void Simulation::modNewBodySpeed(Couple const& decalageMap,
                                  RenderWindow & window) {
    if (objets.size() >= 1) {
        Couple offset;
        float t = objets.size();
        float f = 0.05;
        float limit = 3;

        Vector2i posMouse =
            windowPos2SimulPos(Mouse::getPosition(window), decalageMap);
        offset.x = (posMouse.x - float(objets[t-1]->getX())) * f;
        offset.y = (posMouse.y - float(objets[t-1]->getY())) * f;

        if (offset.x > 0)
            offset.x = min(offset.x, limit);
        else
            offset.x = max(offset.x, -limit);

        if (offset.y > 0)
            offset.y = min(offset.y, limit);
        else
            offset.y = max(offset.y, -limit);

        objets[objets.size()-1]->modVitesse(offset);
    }
}



void Simulation::initTpsCol()
{
    tpsCol.clear();

    vector< double > colonne;

    for(unsigned int i(0);i<tOb;i++)
    {
        for(unsigned int j(0);j<=i;j++)
            colonne.push_back(objets[i]->testCollision(*objets[j]));
        tpsCol.push_back(colonne);
        colonne.clear();
    }
}



void Simulation::computeTpsCol()
{
    for(unsigned int i(0);i<tOb;i++)
        for(unsigned int j(0);j<=i;j++)
            tpsCol[i][j] = objets[i]->testCollision(*objets[j]);
}



void Simulation::gestionChoc()
{
    // on compte le nombre d'elements != de -1.0 dans la matrice et on le
    // stocke dans nb_elts
    unsigned int nb_elts(compte_nb_elts(tpsCol, tOb));

    /* tant que nb_elts != 0
     * on gere la collision correspondant au tpsCol[i][j] le plus petit,
     * on ajoute i et j a la liste des indices des corps deja traites
     * on supprime dans la matrice toutes les colisions qui concernent i et j,
     * tout en decrementant le nb_elts quand il le faut */
    unsigned int k = 0;
    while(nb_elts != 0)
    {
        Couple indices( tpsColMin(tpsCol, tOb));
        objets[indices.x]->gestionChoc( *objets[indices.y],
                                       tpsCol[indices.x][indices.y] );
        ind_corps_traites[k] = indices.x;
        ind_corps_traites[k+1] = indices.y;

        nb_elts = suppression_tps(tpsCol, tOb, nb_elts, indices);
        k += 2;
    }
    indCorpsTraitesSize_ = k;

    // on ordonne la liste des indices des corps deja traite
    // (+ optimise pour l'etape suivante)
    triRapide(ind_corps_traites, 0, k-1);
}



void Simulation::gestionCorpsRestants()
{
    /* on parcours les corps qui n'ont pas deja etes traites (sachant qu'elle
     * est ordonnee, important), et pour chacun d'entre eux :
     * on actualise sa position
     * on actualise sa vitesse
     * on parcours les autres corps
     * s'il a rencontre un autre corps, on le fait rebondir dessus
    */
    // representera les temps de collision entre un corps et tous les autres,
    // en les supposant immobile
    vector< double > tpsCol_immobile;
    bool corpsTraite;
    // indices qui permet de parcourir la liste des corps deja traites
    unsigned int k(0);
    for(unsigned int i(0);i<tOb;i++)
    {
        if(k < indCorpsTraitesSize_)
            if(i != ind_corps_traites[k])
                corpsTraite = false;
            else
            {
                corpsTraite = true;
                k++;
            }
        else
            corpsTraite = false;

        if(!corpsTraite)
        {
            /* si le corps doit cogner un autre corps pendant dt, on traite la
             * collision en le faisant rebondir dessus (on suppose l'autre
             * immobile) (sinon on update juste sa position)
             */
            init_tpsCol_immobile(tpsCol_immobile, i);
            objets[i]->rebondir(tpsCol_immobile, objets);
            tpsCol_immobile.clear();

            objets[i]->updateVitesse();
        }
    }
}



void Simulation::init_tpsCol_immobile(vector< double > & tpsCol_immobile,
                                      unsigned int indice) const
{
    for(unsigned int i(0);i<objets.size();i++)
        tpsCol_immobile.push_back(
                objets[indice]->testCollision_immobile(*objets[i]) );
}

