#include <iostream>
#include <fstream>
#include "Parameters.h"
using namespace std;

map<string, double> Parameters::params;
map<string, double> Parameters::paramsCloud;
map<string, double> Parameters::paramsGrid;

void Parameters::initParamsStorage() {

    params["G"] = 0;
    params["NRJ_CONV_COLLISION"] = 0;
    params["HEAT_CAPACITY"] = 0;
    params["DT"] = 0;
    params["F_RAFFRAICHISSEMENT"] = 0;
    params["FACTEUR_ZOOM"] = 0;
    params["N_DECAL_MAP"] = 0;
    params["ECHELLE"] = 0;
    params["T_FENETRE_X"] = 0;
    params["T_FENETRE_Y"] = 0;
    params["DT_DEBLOQUE"] = 0;
    params["MODE_INIT"] = 0;

    paramsCloud["NOMBRE_CORPS"] = 0;
    paramsCloud["RAYON_BASE"] = 0;
    paramsCloud["RAYON_VARIATION"] = 0;
    paramsCloud["VITESSE_BASE"] = 0;
    paramsCloud["VITESSE_VARIATION"] = 0;
    paramsCloud["V_SPIRALEES"] = 0;
    paramsCloud["ELOIGNEMENT_BASE"] = 0;
    paramsCloud["ELOIGNEMENT_VARIATION"] = 0;

    paramsGrid["BODIES_COUNT"] = 0;
    paramsGrid["RADIUS"] = 0;
    paramsGrid["GAP"] = 0;
}

void Parameters::loadGeneralParameters(string filePath) {
    loadParameters(filePath, params);
}

void Parameters::loadCloudParameters(string filePath) {
    loadParameters(filePath, paramsCloud);
}

void Parameters::loadGridParameters(std::string filePath) {
    loadParameters(filePath, paramsGrid);
}

void Parameters::loadParameters(string filePath, map<string, double> & params_)
{
    ifstream file(filePath.c_str());
    if (file) {
        string word;
        bool isParamName;
        do {
            file >> word;
            isParamName = checkIfParameter(word, params_);
            if (isParamName)
                file >> params_[word];
        } while (word != "__END__");
    }
    else {
        cout << "ERROR : Impossible to open the file " << filePath;
        cout << " in reading mode." << endl;
    }

}

void Parameters::modECHELLE(double echelle) {
    params["ECHELLE"] = echelle;
}

unsigned int Parameters::getGeneralParamNb() {
    return 12;
}

unsigned int Parameters::getCloudParamNb() {
    return 8;
}

unsigned int Parameters::getGridParamNb() {
    return 3;
}

unsigned int Parameters::getNbTotalParams() {
    return getGeneralParamNb() + getCloudParamNb() + getGridParamNb();
}

string Parameters::getDescriptionString(string str) {
    if (str == "G")
        return "gravitation constant (m3.kg-1.s-2)";
    else if (str == "NRJ_CONV_COLLISION")
        return string("proportion of kinetic energy converted into heat during")
               + string(" a collision (0 to 1)");
    else if (str == "HEAT_CAPACITY")
        return "specific heat capacity of the bodies (J.K-1.kg-1)";
    else if (str == "DT")
        return "duration of an iteration (s)";
    else if (str == "F_RAFFRAICHISSEMENT")
        return "screen update delay (ms)";
    else if (str == "FACTEUR_ZOOM")
        return "zoom factor (without unit)";
    else if (str == "N_DECAL_MAP")
        return "number of pixels jumped at arrow keys inputs (pixel)";
    else if (str == "ECHELLE")
        return "number of pixels corresponding to a meter (pixel.m-1";
    else if (str == "T_FENETRE_X")
        return "window width (pixel)";
    else if (str == "T_FENETRE_Y")
        return "window height (pixel)";
    else if (str == "DT_DEBLOQUE")
        return string("if true (1), the computer will run the simulation")
               + string("as fast as it can.\nIf false (0), it will try to")
               + string(" stay close to the DT.");
    else if (str == "MODE_INIT")
        return string("simulation mode. 0 is detailed bodies positioning,")
               + string("\n + 1 is the bodies matrix, 2 is the bodies")
               + string(" cloud.");
    else if (str == "NOMBRE_CORPS")
        return "number of bodies";
    else if (str == "RAYON_BASE")
        return "medium radius (m)";
    else if (str == "RAYON_VARIATION")
        return "radius variation (m)";
    else if (str == "VITESSE_BASE")
        return "medium speed (m.s-1)";
    else if (str == "VITESSE_VARIATION")
        return "speed variation (m.s-1)";
    else if (str == "V_SPIRALEES")
        return string("if true (1), the bodies will orbit around a central")
               + string(" big body");
    else if (str == "ELOIGNEMENT_BASE")
        return "medium distance between the bodies (m)";
    else if (str == "ELOIGNEMENT_VARIATION")
        return "variation of the distance between the bodies (m)";
    else if (str == "BODIES_COUNT")
        return string("number of bodies if the grid (will be rounded down to ")
                + string("the next square number)");
    else if (str == "RADIUS")
        return "radius of the bodies (m)";
    else if (str == "GAP")
        return "gap between the bodies (unit is bodies radius)";
    else
        return "UNDEFINED_DESCRIPTION";
}

bool Parameters::checkIfParameter(string str, map<string, double> & params_) {
    for (auto p=params_.begin(); p!=params_.end(); p++)
        if (p->first == str)
           return true; 
    return false;
}

