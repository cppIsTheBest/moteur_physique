#include "InfosClavier.h"

InfosClavier::InfosClavier() {
        P_pret = true;
}

void InfosClavier::press(sf::Keyboard::Key key) {
    if (key == sf::Keyboard::P) {
        if (P_pret) {
            keys[key] = true;
            P_pret = false;
        }
    }
    else {
        keys[key] = true;
    }
}

void InfosClavier::release(sf::Keyboard::Key key) {
    if (key == sf::Keyboard::P) {
        keys[key] = false;
        P_pret = true;
    }
    else {          
        keys[key] = false;
    }
}

bool InfosClavier::pressed(sf::Keyboard::Key key, double delaiToucheMs) {
    /* indique si un tps suffisament long s'est ecoule depuis le dernier appui
     * sur une touche */
    bool delai_suffisant;

    delai_suffisant = false;
    tTouches2[key] = clock.getElapsedTime();
    if (!delai_suffisant && keys[key]
         && (tTouches2[key]-tTouches1[key]).asMilliseconds() > delaiToucheMs) {
        delai_suffisant = true;
        tTouches1[key] = tTouches2[key];
    }
    
    return keys[key] && delai_suffisant;
}

