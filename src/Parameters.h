#ifndef PARAMETERS_H_INCLUDED
#define PARAMETERS_H_INCLUDED

#include <string>
#include <map>

namespace Parameters {
    void initParamsStorage();
    void loadGeneralParameters(std::string filePath);
    void loadCloudParameters(std::string filePath);
    void loadGridParameters(std::string filePath);
    void loadParameters(std::string filePath,
                          std::map<std::string, double> & params_);
    void modECHELLE(double echelle);

    unsigned int getGeneralParamNb();
    unsigned int getCloudParamNb();
    unsigned int getGridParamNb();
    unsigned int getNbTotalParams();
    std::string getDescriptionString(std::string str);
    bool checkIfParameter(std::string str,
                            std::map<std::string, double> & params_);

    /* general parameters */
    extern std::map<std::string, double> params;
    /* parameters about the bodies cloud mode */
    extern std::map<std::string, double> paramsCloud;
    /* parameters about the bodies grid mode */
    extern std::map<std::string, double> paramsGrid;
};

#endif //PARAMETERS_H_INCLUDED

