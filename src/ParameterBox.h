#ifndef PARAMETERBOX_H_INCLUDED
#define PARAMETERBOX_H_INCLUDED

#include <SFML/Graphics.hpp>
#include <string>
#include "Structures.h"

class ParameterBox {
    public:
        ParameterBox(std::string paramName, sf::Font & font);
        void setPosition(float x, float y);
        void addToString(std::string str);
        /* pop a given number of characters from the string */
        void popString(unsigned int nbChar);
        void display(sf::RenderWindow & window);

        Couple getPosition() const;
        std::string getString() const;

    private:
        unsigned int squareWidth_;
        unsigned int squareHeight_;
        sf::Texture whiteSquareTex_;
        sf::Sprite whiteSquareSprite_;
        float characterSize_;
        sf::Text textParam_;
        sf::Text textDescription_;
        int nbDot_;
        /* -1 if there is no dot in the string */
        int dotIndex_;
};

#endif //PARAMETERBOX_H_INCLUDED

