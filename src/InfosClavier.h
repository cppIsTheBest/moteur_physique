#ifndef INFOCLAVIER_H_INCLUDED
#define INFOCLAVIER_H_INCLUDED

#include <map>
#include <iostream>
#include <SFML/Graphics.hpp>

/* gere les infos sur les touches du clavier, cad si elles sont enfoncees,
 * en tenant compte du delai de repetition des touches */
class InfosClavier
{   
    public:
        InfosClavier();
        void press(sf::Keyboard::Key key);
        void release(sf::Keyboard::Key key); 
        bool pressed(sf::Keyboard::Key key, double delaiToucheMs = 15);
   
   private:
        /* ces booleens indiquent si oui ou non la touche est utilisable */
        std::map<sf::Keyboard::Key, bool> keys;
        /* indique si la touche P est pressable
         * (cad si elle a ete relachee juste avant) */
        bool P_pret;
        /* temps qui permettent de gerer la repetition des evenements
         * des touches */
        sf::Clock clock;
        std::map<sf::Keyboard::Key, sf::Time> tTouches1, tTouches2;
};

#endif // INFOCLAVIER_H_INCLUDED
