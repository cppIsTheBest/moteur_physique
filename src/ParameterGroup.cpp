#include "ParameterGroup.h"
#include "Parameters.h"

using namespace std;
using namespace sf;

ParameterGroup::ParameterGroup(string name, Font & font) :
    rectangle_(Vector2f(Parameters::params["T_FENETRE_X"], 3)),
    text_(name, font, 30)
{
    rectangle_.setFillColor(Color::Green);
    text_.setColor(Color::Green);
}

void ParameterGroup::setPosition(float y) {
    rectangle_.setPosition(0, y);
    text_.setPosition(Parameters::params["T_FENETRE_X"]
            - text_.getGlobalBounds().width - text_.getLocalBounds().left,
            y - text_.getLocalBounds().top + 5);
}

void ParameterGroup::display(sf::RenderWindow & window) {
    window.draw(rectangle_);
    window.draw(text_);
}

Couple ParameterGroup::getPosition() const {
    Couple c;
    c.x = rectangle_.getPosition().x;
    c.y = rectangle_.getPosition().y;
    return c;
}

