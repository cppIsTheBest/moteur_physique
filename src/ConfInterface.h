#ifndef CONFINTERFACE_H_INCLUDED
#define CONFINTERFACE_H_INCLUDED

#include "InfosClavier.h"
#include "Structures.h"
#include "ParameterBox.h"
#include "ParameterGroup.h"
#include "Button.h"

class ConfInterface {
    public:
        ConfInterface(sf::Font & font);

        InterfaceState manageEvents(InfosClavier & infosClavier);
        void display(sf::RenderWindow & window);

    private:
        void cursorManagement(InfosClavier & infosClavier);
        void editParameterBox(InfosClavier & infosClavier);
        void saveParameters();

        sf::Font & font_;
        Button savedButton_;
        bool saved_;

        int yCursor_;
        int nbParameters_;
        sf::CircleShape cursorSprite_;   // circle with 3 sides = triangle
        std::vector<ParameterBox> parametersBox_;
        const int boxSizeY_;
        const int gapBoxY_;
        ParameterGroup generalParamsGroup_;
        ParameterGroup cloudParamsGroup_;
        ParameterGroup gridParamsGroup_;
};

#endif //CONFINTERFACE_H_INCLUDED

