#ifndef SIMULATION_H_INCLUDED
#define SIMULATION_H_INCLUDED

#include <SFML/Graphics.hpp>
#include <vector>
#include "Corps.h"

class Simulation
{
    public:

        /* constructeur de la simulation, recupere les parametres
         * et initialise en consequence les corps */
        Simulation();
        ~Simulation();
        /* cree les images de tous les corps en fonction de leur rayon */
        void creer_img();
        /* calcule l'iteration suivante de la simulation */
        void actualisation();
        /* affiche le nombre d'iterations executees par seconde theoriques
         * et reelles */
        void affiche_performances();

        /* Add a body to the vector. It will be controlled by the mouse
         * until the left button is released */
        void createNewBody(Couple const& decalageMap,
                            sf::RenderWindow & window);
        void modNewBodySpeed(Couple const& decalageMap,
                              sf::RenderWindow & window);

        /* renvoie une reference vers le vector des corps */
        std::vector< Corps* >& getObjects() { return objets; };

        /* indicates if the body at the end of the vector is being created by
           the user */
        bool newBodyExists_;

    private:

        /* initialise les corps selon les infos de placement_corps */
        void init_corps(); 
        /* initialise les corps selon les infos de carre_corps */
        void init_grille_corps();
        /* initialise un nuage de corps selon les parametres contenus
         * dans nuage_corps */
        void init_nuage_corps();

        /* initTpsCol : on cree une matrice triangulaire tpsCol de double
         * de taille objets.size()*objets.size() */
        void initTpsCol();
        /*init_tpsCol : on cree une matrice tpsCol de double de taille
         * objets.size()*objets.size().
         * si il y est suppose y avoir une collision entre les corps d'indices
         * i et j pendant dt :
         *     tpsCol[i][j] contient le temps au bout duquel cette collision
         *     aura lieu
         * sinon :
         *     tpsCol[i][j] contient -1.0; */
        void computeTpsCol();
        /* gestion des collisions, remplis la liste des indices des corps
         * traites */
        void gestionChoc();
        /* gestion des corps qui n'ont pas deja etes traites */
        void gestionCorpsRestants();
        /* calcule les temps de collision du corps i avec tous les autres,
         * en les supposant immobiles */
        void init_tpsCol_immobile(std::vector< double > & tpsCol_immobile,
                                    unsigned int indice) const;

        unsigned int indCorpsTraitesSize_;
        std::vector< unsigned int > ind_corps_traites;
        std::vector< std::vector < double > > tpsCol;
        /* corps de la simulation */
        std::vector< Corps* > objets;
        unsigned int tOb;
        /* nombres d'executions de Simulation::actualisation() */
        unsigned int nb_iterations;
        /* horloge */
        sf::Clock clock;
        /* nb de ms renvoye par SDL_GetTicks() dans le constructeur */
        sf::Time start_time;
};

#endif // SIMULATION_H_INCLUDED

