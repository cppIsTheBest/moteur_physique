#include <iostream>
#include <fstream>
#include <cstring>
using namespace std;
#include <SFML/Graphics.hpp>
using namespace sf;
#include "Simulation.h"
#include "Interface.h"
#include "Parameters.h"

int main ( int argc, char** argv )
{
    // font loading
    sf::Font font;
    if(!font.loadFromFile("DejaVuSans.ttf")) {
        cout << "impossible de charger la police. Le fichier DejaVuSans.ttf ";
        cout << "doit etre present dans le dossier\n" << endl;
    }

    // chargement des parametres
    Parameters::initParamsStorage();
    Parameters::loadGeneralParameters("Data/currentGeneralParams.txt");
    Parameters::loadCloudParameters("Data/currentCloudParams.txt");
    Parameters::loadGridParameters("Data/currentGridParams.txt");

    RenderWindow window(VideoMode(
        (unsigned int)Parameters::params["T_FENETRE_X"],
        (unsigned int)Parameters::params["T_FENETRE_Y"]), "Moteur physique");
    window.setFramerateLimit(60);

    //creation de la simulation et creation des images des corps
    Simulation simulation;
    simulation.creer_img();
    Interface interface(simulation, window, font);
    Clock clock;
    Time tAffich1, tAffich2, tIter1, tIter2;
    tAffich1 = tAffich2 = tIter1 = tIter2 = milliseconds(0);

    window.setKeyRepeatEnabled(false);
    Event event;
    while(!interface.getDone())
    {
        /* si un temps dt s'est ecoule et qu'on est pas en pause, on calcule
         * l'iteration suivante. Ce delai est egal a DT par default
         * (pour que la simulation soit effectuee "en temps reel" et a la
         * meme vitesse sur tous les ordinateurs du moment qu'ils ont une
         * puissance suffisante), mais ceci peut etre change pendant la 
         * simulation */
        tIter2 = clock.getElapsedTime();
        if((((tIter2-tIter1).asSeconds() > interface.getCurrentDelay())
           || Parameters::params["DT_DEBLOQUE"]) && !interface.getPause())
        {
            simulation.actualisation();
            tIter1 = tIter2;
        }

        //enregistrement des evenements par l'interface
        while(window.pollEvent(event)) {
            interface.recordKeyboardEvents(event);
            interface.manageMouseEvents(event);
        }

        interface.manageKeyboardEvents(); 

        //gestion de l'affichage
        tAffich2 = clock.getElapsedTime();
        if((tAffich2-tAffich1).asMilliseconds()
                > (int)Parameters::params["F_RAFFRAICHISSEMENT"])
        {
            window.clear(Color(0, 0, 0));
            interface.display();
            window.display();
            tAffich1 = tAffich2;
        }
    }

    window.close();
    simulation.affiche_performances();
    return 0;
}

