#include <iostream>
#include <fstream>
#include "ConfInterface.h"
#include "Parameters.h"

using namespace std;
using namespace sf;

ConfInterface::ConfInterface(sf::Font & font) :
    font_(font),
    saved_(false),
    yCursor_(0),
    nbParameters_(Parameters::getNbTotalParams()),
    cursorSprite_(17, 3),
    boxSizeY_(30),
    gapBoxY_(50),
    generalParamsGroup_("GENERAL PARAMETERS", font),
    cloudParamsGroup_("BODIES CLOUD PARAMETERS", font),
    gridParamsGroup_("BODIES GRID PARAMETERS", font)
{
    // setup button
    savedButton_.init(font, "SAVED", 20,
                        Color(206, 241, 192), Color(51, 181, 0));
    savedButton_.setPosition(
            Parameters::params["T_FENETRE_X"]-savedButton_.getSizeBG().x,
            Parameters::params["T_FENETRE_Y"]-savedButton_.getSizeBG().y);

    // setup cursor
    cursorSprite_.setFillColor(sf::Color(255, 126, 0));
    cursorSprite_.setPosition(40, 10);
    cursorSprite_.setRotation(90);

    // setup parameters boxes and parameters group indicators
    int i = 0;
    auto p=Parameters::params.begin();
    generalParamsGroup_.setPosition(i*(gapBoxY_+boxSizeY_));
    while (p!=Parameters::paramsGrid.end()) {
        parametersBox_.push_back(ParameterBox(p->first, font_));
        parametersBox_[i].setPosition(50, 10 + i*(gapBoxY_+boxSizeY_));
        p++;
        if (p==Parameters::params.end()) {
            cloudParamsGroup_.setPosition(i*(gapBoxY_+boxSizeY_)+boxSizeY_+17);
            p = Parameters::paramsCloud.begin();
        }
        else if (p==Parameters::paramsCloud.end()) {
            gridParamsGroup_.setPosition(i*(gapBoxY_+boxSizeY_)+boxSizeY_+17);
            p = Parameters::paramsGrid.begin();
        }
        i++;
    }
}

InterfaceState ConfInterface::manageEvents(InfosClavier & infosClavier) {
    cursorManagement(infosClavier);
   
    if (infosClavier.pressed(sf::Keyboard::Escape)) {
        // quit editing conf without saving if escape key pressed
        return RUNNING;
    } else if ((infosClavier.pressed(sf::Keyboard::LControl)
                    || infosClavier.pressed(sf::Keyboard::RControl))
                && (infosClavier.pressed(sf::Keyboard::S))) {
        // save editing conf if Ctrl + S is pressed
        saveParameters();
        return EDITING_CONF;
    } else {
        // if numbers keys pressed, edit strings of parameter boxes
        editParameterBox(infosClavier);
        return EDITING_CONF;
    }
}

void ConfInterface::cursorManagement(InfosClavier & infosClavier) {
    // check if arrow key up or down are pressed
    float offsetPosCursor(0);
    if (infosClavier.pressed(sf::Keyboard::Down, 150)) {
        yCursor_ ++;
        offsetPosCursor = boxSizeY_ + gapBoxY_;
    }
    if (infosClavier.pressed(sf::Keyboard::Up, 150)) {
        yCursor_ --;
        offsetPosCursor = -(boxSizeY_ + gapBoxY_);
    }

    // limit the lower bound of the cursor selection to 0
    if (yCursor_ < 0) {
        yCursor_ = 0;
    }
    // limit the upper bound of the cursor selection to nbParameters_-1
    else if (yCursor_ > nbParameters_ - 1) {
        yCursor_ = nbParameters_ - 1;
    }
    // update the position of the elements (cursor sprite and boxes)
    else if (offsetPosCursor != 0) {
        bool beyondBottom = (cursorSprite_.getPosition().y + offsetPosCursor)
             > (Parameters::params["T_FENETRE_Y"]
                - cursorSprite_.getGlobalBounds().height);
        bool beyondTop = (cursorSprite_.getPosition().y + offsetPosCursor < 0); 

        /* move up all the param boxes if cursor is at the bottom,
         * or move down all the param boxes if cursor is at the top */
        if (beyondBottom || beyondTop) {
            float x, y;
            for (unsigned int i=0; i<parametersBox_.size(); i++) {
                x = parametersBox_[i].getPosition().x;
                y = parametersBox_[i].getPosition().y;
                parametersBox_[i].setPosition(x, y-offsetPosCursor);
            }
            y = generalParamsGroup_.getPosition().y;
            generalParamsGroup_.setPosition(y-offsetPosCursor);
            y = cloudParamsGroup_.getPosition().y;
            cloudParamsGroup_.setPosition(y-offsetPosCursor);
            y = gridParamsGroup_.getPosition().y;
            gridParamsGroup_.setPosition(y-offsetPosCursor);
        }
        // move cursor sprite if no problem
        else {
            cursorSprite_.move(0, offsetPosCursor);
        }
    }
}

void ConfInterface::editParameterBox(InfosClavier & infosClavier) {
    bool modifs = true;

    if (infosClavier.pressed(sf::Keyboard::Num0, 150)
            || infosClavier.pressed(sf::Keyboard::Numpad0, 150)) {
        parametersBox_[yCursor_].addToString("0");
    } else if (infosClavier.pressed(sf::Keyboard::Num1, 150)
            || infosClavier.pressed(sf::Keyboard::Numpad1, 150)) {
        parametersBox_[yCursor_].addToString("1");
    } else if (infosClavier.pressed(sf::Keyboard::Num2, 150)
            || infosClavier.pressed(sf::Keyboard::Numpad2, 150)) {
        parametersBox_[yCursor_].addToString("2");
    } else if (infosClavier.pressed(sf::Keyboard::Num3, 150)
            || infosClavier.pressed(sf::Keyboard::Numpad3, 150)) {
        parametersBox_[yCursor_].addToString("3");
    } else if (infosClavier.pressed(sf::Keyboard::Num4, 150)
            || infosClavier.pressed(sf::Keyboard::Numpad4, 150)) {
        parametersBox_[yCursor_].addToString("4");
    } else if (infosClavier.pressed(sf::Keyboard::Num5, 150)
            || infosClavier.pressed(sf::Keyboard::Numpad5, 150)) {
        parametersBox_[yCursor_].addToString("5");
    } else if (infosClavier.pressed(sf::Keyboard::Num6, 150)
            || infosClavier.pressed(sf::Keyboard::Numpad6, 150)) {
        parametersBox_[yCursor_].addToString("6");
    } else if (infosClavier.pressed(sf::Keyboard::Num7, 150)
            || infosClavier.pressed(sf::Keyboard::Numpad7, 150)) {
        parametersBox_[yCursor_].addToString("7");
    } else if (infosClavier.pressed(sf::Keyboard::Num8, 150)
            || infosClavier.pressed(sf::Keyboard::Numpad8, 150)) {
        parametersBox_[yCursor_].addToString("8");
    } else if (infosClavier.pressed(sf::Keyboard::Num9, 150)
            || infosClavier.pressed(sf::Keyboard::Numpad9, 150)) {
        parametersBox_[yCursor_].addToString("9");
    } else if (infosClavier.pressed(sf::Keyboard::Comma, 150)
            || infosClavier.pressed(sf::Keyboard::Period, 150)) {
        parametersBox_[yCursor_].addToString(".");
    } else if (infosClavier.pressed(sf::Keyboard::BackSpace, 100)) {
        parametersBox_[yCursor_].popString(1);
    } else {
        modifs = false;
    }

    if (modifs)
        saved_ = false;
}

void ConfInterface::saveParameters() {
    string str;

    // if there are not enough fields, we don't save
    if (parametersBox_.size() != Parameters::getNbTotalParams()) {
        cout << "ERROR while saving conf : the number of fields does not match";
        cout << " the number of existing parameters" << endl;
        return;
    }

    // if a parameter is not defined, we don't save
    bool paramsDefined = true;
    for (unsigned int i=0; i<Parameters::getNbTotalParams(); i++)
        if (parametersBox_[i].getString().size() == 0)
            paramsDefined = false;
    if (!paramsDefined) {
        cout << "ERROR while saving conf : a field was left empty" << endl;
        return;
    }

    ofstream fileParam;
    fileParam.open("Data/currentGeneralParams.txt");
    int i=0;
    for (auto p=Parameters::params.begin(); p!=Parameters::params.end(); p++) {
        str = parametersBox_[i].getString();
        fileParam << p->first << " ";
        fileParam << str << endl;
        i++;
    }
    fileParam << "__END__" << endl;
    fileParam.close();

    ofstream fileCloud;
    fileCloud.open("Data/currentCloudParams.txt");
    for (auto p=Parameters::paramsCloud.begin();
                p!=Parameters::paramsCloud.end(); p++) {
        str = parametersBox_[i].getString();
        fileCloud << p->first << " ";
        fileCloud << str << endl;
        i++;
    }
    fileCloud << "__END__" << endl;
    fileCloud.close();

    ofstream fileGrid;
    fileGrid.open("Data/currentGridParams.txt");
    for (auto p=Parameters::paramsGrid.begin();
                p!=Parameters::paramsGrid.end(); p++) {
        str = parametersBox_[i].getString();
        fileGrid << p->first << " ";
        fileGrid << str << endl;
        i++;
    }
    fileGrid << "__END__" << endl;
    fileGrid.close();

    saved_ = true;
}

void ConfInterface::display(sf::RenderWindow & window) {
    window.draw(cursorSprite_);
    for (unsigned int i(0); i<parametersBox_.size(); i++) {
        parametersBox_[i].display(window);
    }
    generalParamsGroup_.display(window);
    cloudParamsGroup_.display(window);
    gridParamsGroup_.display(window);
    if (saved_)
        savedButton_.display(window);
}

