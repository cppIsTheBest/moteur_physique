#ifndef STRUCTURES_H_INCLUDED
#define STRUCTURES_H_INCLUDED

#include <string>

enum InterfaceState { RUNNING, EDITING_CONF  };

struct Couple
{
    float x, y;
};

struct MatPassage
{
    double x11, x12, x21, x22;
    /*represente une matrice de passage | x11 x12 |
                                        | x21 x22 |
    */
};

struct VitessePolaire
{
    double v1, v2, a1, a2;
};

struct VitesseCartesienne
{
    double vx1, vy1, vx2, vy2;
};

#endif // STRUCTURES_H_INCLUDED

