#include <iostream>
#include "Button.h"


Button::Button() {
}

Button::Button(sf::Font & font, std::string text, int sizePixelText,
                sf::Color frColor, sf::Color bgColor)
{
    init(font, text, sizePixelText, frColor, bgColor);
}

Button::Button(Button const& button) :
    m_text(button.getText()),
    m_backgroundShape(button.getBackgroundShape())
{
    m_font = new sf::Font(button.getFont());
    m_text.setFont(*m_font);
    m_backgroundMargin = button.getBackgroundMargin();
    m_alarmState = button.getAlarmState();
}

void Button::init(sf::Font & font, std::string text, int sizePixelText,
                   sf::Color frColor, sf::Color bgColor) {
    m_font = &font;

    //init m_text
    m_text = sf::Text(text, *m_font, sizePixelText);
    m_text.setColor(frColor);

    //init background
    m_backgroundShape.setFillColor(bgColor);
    m_backgroundMargin = 5;
    setSizeBackground();

    m_alarmState = false;
}

void Button::display(sf::RenderWindow & window) {
    window.draw(m_backgroundShape);
    window.draw(m_text);
}

void Button::setPosition(int x, int y) {
    m_backgroundShape.setPosition(x, y);
    m_text.setPosition(x+m_backgroundMargin-m_text.getLocalBounds().left,
                       y+m_backgroundMargin-m_text.getLocalBounds().top);
}

void Button::setString(std::string str) {
    m_text.setString(str);
    setSizeBackground();
}

bool Button::isClicked(sf::Event const& event) const {
    if (event.type == sf::Event::MouseButtonPressed
            && event.mouseButton.button == sf::Mouse::Left) {
        return event.mouseButton.x >= m_text.getPosition().x
                && event.mouseButton.x <
                    m_text.getPosition().x + m_text.getLocalBounds().width
                && event.mouseButton.y >= m_text.getPosition().y
                && event.mouseButton.y <
                    m_text.getPosition().y + m_text.getLocalBounds().height;
    }
    else
        return false;
}

Couple Button::getSizeBG() const {
    Couple sizeButton;
    sizeButton.x = m_backgroundShape.getGlobalBounds().width;
    sizeButton.y = m_backgroundShape.getGlobalBounds().height;
    return sizeButton;
}

Couple Button::getSizeText() const {
    Couple sizeButton;
    sizeButton.x = m_text.getGlobalBounds().width;
    sizeButton.y = m_text.getGlobalBounds().height;
    return sizeButton;
}

void Button::setSizeBackground() {
    float s_x(m_text.getGlobalBounds().width+m_backgroundMargin*2);
    float s_y(m_text.getGlobalBounds().height+m_backgroundMargin*2);
    m_backgroundShape.setSize(sf::Vector2f(s_x, s_y));
}

