#include "ParameterBox.h"
#include "Parameters.h"
#include <sstream>
#include <iostream>

using namespace std;
using namespace sf;

ParameterBox::ParameterBox(string paramName, Font & font) {
    squareWidth_ = 200;
    squareHeight_ = 30;
    whiteSquareSprite_.setTexture(whiteSquareTex_);
    whiteSquareSprite_.setTextureRect(IntRect(0, 0,
                                          squareWidth_, squareHeight_));
    whiteSquareSprite_.setColor(Color(128, 128, 128));

    characterSize_ = squareHeight_ - 4;
    textParam_.setPosition(0, 0);
    textParam_.setFont(font);
    textParam_.setCharacterSize(characterSize_);
    textParam_.setColor(Color::White);

    // set the param string
    map<string, double> * prms;
    if (Parameters::checkIfParameter(paramName, Parameters::params)) {
        prms = &(Parameters::params);
    }
    else if (Parameters::checkIfParameter(paramName, Parameters::paramsCloud)) {
        prms = &(Parameters::paramsCloud);
    }
    else {
        prms = &(Parameters::paramsGrid);
    }
    double paramValue = (*prms)[paramName];
    std::ostringstream oss;
    oss << paramValue;
    string paramStr = oss.str();
    textParam_.setString(paramStr);
    string::size_type dotPos = paramStr.find(".");
    if (dotPos != string::npos) {
        nbDot_ = 1;
        dotIndex_ = dotPos;
    }

    textDescription_.setPosition(0, 0);
    textDescription_.setFont(font);
    textDescription_.setCharacterSize(characterSize_);
    textDescription_.setColor(Color::White);
    textDescription_.setString(Parameters::getDescriptionString(paramName));

    nbDot_ = 0;
    dotIndex_ = -1;
}

void ParameterBox::setPosition(float x, float y) {
    // set square position
    whiteSquareSprite_.setPosition(x, y);
    // set param text position
    float gap = 0;
    if (squareHeight_ > characterSize_)
        gap = (squareHeight_-textParam_.getGlobalBounds().height) / 2;
    textParam_.setPosition(x - textParam_.getLocalBounds().left + gap,
                            y - textParam_.getLocalBounds().top + gap);
    // set description string position
    textDescription_.setPosition(
        x - textDescription_.getLocalBounds().left + squareWidth_ + 15,
        y - textDescription_.getLocalBounds().top);
}

void ParameterBox::addToString(string str) {
    bool authorized = false;
    int sizeText = textParam_.getString().getSize();
    if (str == "." && nbDot_ == 0 && sizeText > 0) {
        authorized = true;
        nbDot_++;
        dotIndex_ = sizeText;
    }
    else if (str == "0" || str == "1" || str == "2" || str == "3" || str == "4"
             || str == "5" || str == "6" || str == "7" || str == "8"
             || str == "9") {
        authorized = true;
    }
    else {
        authorized = false;
    }

    if (authorized)
        textParam_.setString(textParam_.getString() + str);
}

void ParameterBox::popString(unsigned int nbChar) {
    String str = textParam_.getString();
    if (nbChar > 0 && nbChar <= str.getSize()) {
        textParam_.setString(str.substring(0, str.getSize()-nbChar));
        //if a dot is popped, update infos about the dot
        if (dotIndex_ >= (int)(str.getSize()-nbChar)
                && dotIndex_ < (int)str.getSize()) {
            nbDot_ = 0;
            dotIndex_ = 0;
        }
    }
}

void ParameterBox::display(RenderWindow & window) {
    window.draw(whiteSquareSprite_);
    window.draw(textParam_);
    window.draw(textDescription_);
}

Couple ParameterBox::getPosition() const {
    Couple c;
    c.x = whiteSquareSprite_.getPosition().x;
    c.y = whiteSquareSprite_.getPosition().y;
    return c;
}

std::string ParameterBox::getString() const {
    return textParam_.getString().toAnsiString();
}

