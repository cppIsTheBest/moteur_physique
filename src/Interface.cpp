#include <iostream>
#include <sstream>
#include "Interface.h"
#include "Parameters.h"

using namespace std;
using namespace sf;

#define arrondi(a) int(a + 0.5)

Interface::Interface(Simulation & simulation, RenderWindow& window_,
                        sf::Font & font) : 
    state_(RUNNING), 
    simulation_(simulation),
    objets(simulation.getObjects()),
    window(window_),
    confInterface_(font),
    imgCroix1(Vector2f(10,2)), 
    imgCroix2(Vector2f(2,10)),
    done(false),
    pause(false),
    pausedByEditing_(false),
    speedTextUpToDate_(false),
    currentDelay_(Parameters::params["DT"])
{
    decalageMap.x = 0;
    decalageMap.y = 0;

    imgCroix1.setFillColor(Color(255,11,11));
    imgCroix2.setFillColor(Color(255,11,11));

    speedText_.setFont(font);
    speedText_.setCharacterSize(24);
    speedText_.setColor(Color::White);

    fullSpeedText_.setFont(font);
    fullSpeedText_.setCharacterSize(24);
    fullSpeedText_.setColor(Color::White);
    fullSpeedText_.setString("MAX SPEED");
    float x = Parameters::params["T_FENETRE_X"]
                - fullSpeedText_.getGlobalBounds().width;
    float y = Parameters::params["T_FENETRE_Y"]
                - fullSpeedText_.getGlobalBounds().height;
    fullSpeedText_.setPosition(x, y);

    buttonRunning_.init(font, "RUNNING", 30, Color(206, 241, 192),
            Color(51, 181, 0));
    buttonRunning_.setPosition(
            Parameters::params["T_FENETRE_X"]-buttonRunning_.getSizeBG().x, 0);
    buttonPaused_.init(font, "PAUSED", 30, Color(246, 133, 127),
            Color(234, 15, 4));
    buttonPaused_.setPosition(
            Parameters::params["T_FENETRE_X"]-buttonPaused_.getSizeBG().x, 0);
}



void Interface::recordKeyboardEvents(Event const& event)
{
    switch (event.type) {
        /* si on un evenement de fermeture a eu lieu on ferme la fenetre et on
         * quitte le programme */
        case Event::Closed:
            window.close();
            done = true;
            break;
        case Event::KeyPressed:
            //enregistrement des appuis sur les touches
            if (event.key.code == Keyboard::Escape && state_ == RUNNING) {
                /* si appui sur echap, on ferme la fenetre
                 * et on quitte le programme */
                window.close();
                done = true;
                break;
            } else {
                infosClavier.press(event.key.code);
            }
            break;
        //enregistrement des relachement des touches
        case Event::KeyReleased:
            infosClavier.release(event.key.code);
            break;
        default:
            break;
    }

    if (infosClavier.pressed(Keyboard::P, 0)) {
        if (pause)
            pause = false;
        else
            pause = true;
    }
}



void Interface::manageKeyboardEvents() {
    if ((infosClavier.pressed(Keyboard::LControl)
            || infosClavier.pressed(Keyboard::RControl))
        && (infosClavier.pressed(Keyboard::E))) {
        state_ = EDITING_CONF;
        if (!pause)
            pausedByEditing_ = true;
        pause = true;
    }

    if (state_ == RUNNING) {
        gestionDeplacementMap();
        gestionZoom();
        speedManagement();
    } else if(state_== EDITING_CONF) {
        state_ = confInterface_.manageEvents(infosClavier);
        if (state_ == RUNNING) {
            if (pausedByEditing_)
                pause = false;
            pausedByEditing_ = false;
        }
    }
}



void Interface::manageMouseEvents(Event const& event) {
    if (state_ == RUNNING) {
        BodyCreator::manageEvents(simulation_, event, decalageMap, window);
    }
}



void Interface::gestionDeplacementMap()
{ 
    if(infosClavier.pressed(Keyboard::Up))
        decalageMap.y += Parameters::params["N_DECAL_MAP"];
    if(infosClavier.pressed(Keyboard::Down))
        decalageMap.y -= Parameters::params["N_DECAL_MAP"];
    if(infosClavier.pressed(Keyboard::Left))
        decalageMap.x += Parameters::params["N_DECAL_MAP"];
    if(infosClavier.pressed(Keyboard::Right))
        decalageMap.x -= Parameters::params["N_DECAL_MAP"];

    if(infosClavier.pressed(Keyboard::C, 0)) {
        decalageMap.x = 0;
        decalageMap.y = 0;
        Parameters::modECHELLE(1);
        for(unsigned int i(0); i<objets.size(); i++)
            objets[i]->delete_img();
    }
}



void Interface::gestionZoom()
{
    /* s'il y a eu un appui sur z/s, 
       on modifie l'echelle et le decalage de la map, et on supprime les 
       images des objets qui sont obsoletes */
    bool modifyZoom(false);    
    double facteur; 
    
    if (infosClavier.pressed(Keyboard::Z)) {
        facteur = Parameters::params["FACTEUR_ZOOM"];
        modifyZoom = true;
    }
    if (infosClavier.pressed(Keyboard::S)) {
        facteur = 1/Parameters::params["FACTEUR_ZOOM"];
        modifyZoom = true;
    }
    
    if (modifyZoom) {
        Parameters::modECHELLE(Parameters::params["ECHELLE"]*facteur);
        decalageMap.x *= facteur;
        decalageMap.y *= facteur;
        for(unsigned int i(0);i<objets.size();i++)
            objets[i]->delete_img();
    }
}



void Interface::speedManagement() {
    double f = 1.04;
    if (infosClavier.pressed(Keyboard::T, 50) && currentDelay_/f > 0) {
        currentDelay_ /= f;
        speedTextUpToDate_ = false;
    }
    else if (infosClavier.pressed(Keyboard::R, 50)) {
        currentDelay_ *= f;
        speedTextUpToDate_ = false;
    }
}



void Interface::display() {
    if (state_ == RUNNING)
        displaySimulation();
    else
        confInterface_.display(window);
}



void Interface::displaySimulation()
{
    //affichage des corps
    for(unsigned int i(0);i<objets.size();i++)
    {
        if (simulation_.getObjects().size() != 2) {
        }
        //si le corps est dans l'ecran on l'affiche
        if( objets[i]->inScreen(decalageMap) )
        {
            //creation de l'image (ne sera fait que si l'image = NULL)
            objets[i]->create_img(Parameters::params["ECHELLE"]);
            //affichage du corps
            objets[i]->affichage(window, decalageMap);
        }
    }

    /* affichage de la croix rouge (composee de deux images rectangulaires)
     * au point de coordonees (0,0) */
    posCroix.x = decalageMap.x - imgCroix1.getSize().x/2
                 + Parameters::params["T_FENETRE_X"]/2;
    posCroix.y = decalageMap.y - imgCroix1.getSize().y/2
                 + Parameters::params["T_FENETRE_Y"]/2;
    if(posCroix.x > 0 && posCroix.y > 0
       && posCroix.x + imgCroix1.getSize().x*2
                    < arrondi(Parameters::params["T_FENETRE_X"])
       && posCroix.y + imgCroix1.getSize().y*2
                    < arrondi(Parameters::params["T_FENETRE_Y"])) {
        imgCroix1.setPosition(posCroix.x, posCroix.y);
        window.draw(imgCroix1);
    }
    posCroix.x = decalageMap.x - imgCroix2.getSize().x/2
                + Parameters::params["T_FENETRE_X"]/2;
    posCroix.y = decalageMap.y - imgCroix2.getSize().y/2
                + Parameters::params["T_FENETRE_Y"]/2;
    if(posCroix.x > 0 && posCroix.y > 0
       && posCroix.x + imgCroix2.getSize().x*2
                    < arrondi(Parameters::params["T_FENETRE_X"])
       && posCroix.y + imgCroix2.getSize().y*2
                    < arrondi(Parameters::params["T_FENETRE_Y"]) ) {
        imgCroix2.setPosition(posCroix.x, posCroix.y);
        window.draw(imgCroix2);
    }

    // display paused or running button
    if (pause)
        buttonPaused_.display(window);
    else
        buttonRunning_.display(window);

    if (Parameters::params["DT_DEBLOQUE"])
        window.draw(fullSpeedText_);
    else
        displaySpeed();
}


void Interface::displaySpeed() {
    if (!speedTextUpToDate_) {
        // create speed string
        string str;//= + " / ";
        ostringstream oss;
        oss << 1/currentDelay_;
        str += oss.str();
        str += " it / s";

        // setup speed text
        speedText_.setString(str);
        float x = Parameters::params["T_FENETRE_X"]
                  - speedText_.getGlobalBounds().width
                  - speedText_.getLocalBounds().left;
        float y = Parameters::params["T_FENETRE_Y"]
                  - speedText_.getGlobalBounds().height
                  - speedText_.getLocalBounds().top;
        speedText_.setPosition(x, y);
        speedTextUpToDate_ = true;
    }
    window.draw(speedText_);
}


bool Interface::getDone() const {
    return done;
}


bool Interface::getPause() const {
    return pause;
}

