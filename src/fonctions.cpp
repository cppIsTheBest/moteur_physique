#include <vector>
#include <cmath>
#include "Parameters.h"
#include "Corps.h"
#include "Structures.h"
#include "constantes.h"

/* retourne le nb d'elements differents de -1.0 de la matrice triangulaire
 * superieure tpsCol */
unsigned int compte_nb_elts(std::vector< std::vector< double > > const& tpsChoc,
                            unsigned int taille)
{
    unsigned int nb_elts(0);

    for(unsigned int i(0);i<taille;i++)
        for(unsigned int j(0);j<=i;j++)
            if(tpsChoc[i][j] > 0)
                nb_elts ++;

    return nb_elts;
}

/* retourne les indices des deux objets qui se heuteront en premier */
Couple tpsColMin(std::vector< std::vector< double > > const& tpsCol,
                 unsigned int taille)
{
    double tps_min(Parameters::params["DT"] + 1);
    Couple indices;
    indices.x = 0;
    indices.y = 0;

    for(unsigned int i(0);i<taille;i++)
        for(unsigned int j(0);j<=i;j++)
            if(tpsCol[i][j] < tps_min && tpsCol[i][j] > 0)
            {
                tps_min = tpsCol[i][j];
                indices.x = i;
                indices.y = j;
            }

    return indices;
}

/* supprime toutes les colisions qui concernent i et j dans la matrice
 * triangulaire superieure tpsCol, tout en decrementant le nb_elts quand il le
 * faut, et retourne nb_elts */
unsigned int suppression_tps(std::vector< std::vector< double > > & tpsCol,
                                unsigned int taille, unsigned int nb_elts,
                                Couple const& indices)
{
    for(unsigned int i(0);i<taille;i++)
    {
        for(unsigned int j(0);j<=i;j++)
        {
            if((i == indices.x || i == indices.y
                || j == indices.x || j == indices.y)
               && tpsCol[i][j] > 0)
            {   nb_elts --;
                tpsCol[i][j] = -1.0;
            }
        }
    }

    return nb_elts;
}

/* sert a la fonction tri rapide */
void echanger(std::vector< unsigned int > & tableau, int a, int b)
{
    unsigned int temp = tableau[a];
    tableau[a] = tableau[b];
    tableau[b] = temp;
}

/* trie le vector avec l'algorithme du tri rapide (c'est pas moi qui l'ai ecrit celle la :D ) */
void triRapide(std::vector< unsigned int > & tableau, int debut, int fin)
{
    /* Si le tableau est de longueur nulle, il n'y a rien � faire. */
    if(debut >= fin)
        return;

    int gauche = debut-1;
    int droite = fin+1;
    const unsigned int pivot = tableau[debut];

    /* Sinon, on parcourt le tableau, une fois de droite � gauche, et une
       autre de gauche � droite, � la recherche d'�l�ments mal plac�s,
       que l'on permute. Si les deux parcours se croisent, on arr�te. */
    while(1)
    {
        do droite--; while(tableau[droite] > pivot);
        do gauche++; while(tableau[gauche] < pivot);

        if(gauche < droite)
            echanger(tableau, gauche, droite);
        else
            break;
    }

    /* Maintenant, tous les �l�ments inf�rieurs au pivot sont avant ceux
       sup�rieurs au pivot. On a donc deux groupes de cases � trier. On utilise
       pour cela... la m�thode quickSort elle-m�me ! */
    triRapide(tableau, debut, droite);
    triRapide(tableau, droite+1, fin);
}

/* renvoie l'argument du vecteur de coordonnees (x; y) si (x; y) != (0; 0)
 * et une valeur sans reelle signification (1) sinon */
double calculAngleVecteur(double x, double y)
{
    if(x == 0 && y == 0)
        /* parce qu'il faut bien renvoyer qqchose, et si (x,y) = (0,0)
         * le resultat ne sera pas utile de toute fa�on */
        return 1;
    else
    {   double norme(sqrt(pow(x, 2) + pow(y, 2)));
        double alpha(acos(x/norme));
        if (y < 0)
            alpha = -alpha;

        return alpha;
    }
}

/* renvoie le produit scalaire des vecteurs de coordonnees (x1; y1)
 * et (x2; y2) */
double prodScalaire(double x1, double y1, double x2, double y2)
{
    return x1*x2 + y1*y2;
}

/* renvoie le produit scalaire du vecteur (x; y) avec lui-meme */
double prodScalaireCarre(double x, double y)
{
    return x*x + y*y;
}

/* ax�+bx+c = 0 renvoie la plus petite solution positive si min_max = 1
 * ou la plus grande sinon. si pas de solution, renvoie -1. */
double solPolynome(double a, double b, double c, unsigned int min_max) {
    if(a == 0 && b == 0)
        return -1;
    else if(a == 0 && b != 0)
    {   if(-c/b > 0)
            return -c/b;
        else
            return -1;
    }

    double delta(pow(b, 2) - 4*a*c);

    if(delta < 0)
        return -1;
    else if(delta == 0)
    {   if(-b/a > 0)
            return -b/(2*a);
        else
            return -1;
    }
    else
    {   double t1((-b - sqrt(delta))/(2*a));
        double t2((-b + sqrt(delta))/(2*a));
        if(t1 >= 0 && t2 >= 0)
        {   if((t1 < t2 && min_max == 1) || (t1 > t2 && min_max != 1))
                return t1;
            else if((t1 < t2 && min_max != 1) || (t1 > t2 && min_max == 1))
                return t2;
            else
                return t2;
        }
        else if(t1 >= 0)
            return t1;
        else if(t2 >= 0)
            return t2;
        else
            return -1;
    }
}



/* genere un double aleatoire compris entre a et b (ecart minimal entre deux
 * nombres generes = (b-a)/RAND_MAX) */
double frand(double a, double b)
{
    if(a < 0)
        a = 0;
    if(b < 0)
        b = 0;
    return ( rand()/(double)RAND_MAX ) * (b-a) + a;
}



/* fonction qui renvoie l'etat suivant le precedent */
Etat suivant(Etat etat)
{
    if(etat == HD)
        return D;
    else
        return Etat(etat+1); 
}



sf::Vector2i windowPos2SimulPos(sf::Vector2i const& windowPos,
                                Couple const& decalageMap) {
    int x = (windowPos.x - decalageMap.x - Parameters::params["T_FENETRE_X"]/2)
             / Parameters::params["ECHELLE"];
    int y = (windowPos.y - decalageMap.y - Parameters::params["T_FENETRE_Y"]/2)
             / Parameters::params["ECHELLE"];
    return sf::Vector2i(x, y);
}

