#ifndef PARAMETERGROUP_H_INCLUDED
#define PARAMETERGROUP_H_INCLUDED

#include <SFML/Graphics.hpp>
#include <string>
#include "Structures.h"

/* An indicator that can be displayed in edit conf mode, to indicate that the
 * following parameters belong to the same parameter group */
class ParameterGroup {
    public:
        ParameterGroup(std::string name, sf::Font & font);
        void setPosition(float y);
        void display(sf::RenderWindow & window);

        Couple getPosition() const;

    private:
        sf::RectangleShape rectangle_;
        sf::Text text_;
};

#endif //PARAMETERGROUP_H_INCLUDED

