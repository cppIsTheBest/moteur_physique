#include "BodyCreator.h"
#include <iostream>

using namespace sf;
using namespace std;

bool BodyCreator::newBodyExists;

void BodyCreator::init() {
    newBodyExists = false;
}

void BodyCreator::manageEvents(Simulation & simulation, Event const& event,
                            Couple const& decalageMap, RenderWindow & window) {
    switch (event.type) {
        case Event::MouseButtonPressed:
            if (event.mouseButton.button == Mouse::Left) {
                simulation.createNewBody(decalageMap, window);
                newBodyExists = true;
            }
            break;
        case Event::MouseButtonReleased:
            if (event.mouseButton.button == Mouse::Left
                    && newBodyExists) {
                newBodyExists = false;
            }
            break; 
        case Event::MouseMoved:
            if (newBodyExists)
                simulation.modNewBodySpeed(decalageMap, window);
            break;
        default:
            break;
    }
}

