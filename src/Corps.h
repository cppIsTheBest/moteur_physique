#ifndef CORPS_H_INCLUDED
#define CORPS_H_INCLUDED

#include <SFML/Graphics.hpp>
#include <vector>
#include "Structures.h"

/* un corps est un disque de rayon r */
class Corps
{
    public:

        Corps(double x0, double y0, double vx0, double vy0, double r0,
                unsigned int id0);
        ~Corps();
        /* cree l'image du corps en fonction de son rayon
         * et de l'echelle actuelle */
        void create_img(double echelle);
        /* libere l'espace alloue pour l'image */
        void delete_img();

        /* on calcule la nouvelle position a partir de la vitesse courante */
        void updatePosition();
        /* on calcule la nouvelle vitesse a partir de l'acceleration courante */
        void updateVitesse();
        /* add the given offset to the body's speed */
        void modVitesse(Couple const& offset);
        /* on calcule la nouvelle acceleration subie par le corps
         * (induite par tous les autres corps) */
        void updateAcceleration(std::vector< Corps* > const& objets, double G);

        /* Il y a choc avec c. on calcule la nouvelle vitesse des deux corps
         * apres le choc */
        void gestionChoc(Corps & c, double t);
        /* Meme chose mais le deuxieme corps est considere immobile,
         * et on ne modifie que la vitesse du premier */
        void gestionChoc_immobile(Corps const& c, double t);

        /* calcul de la base adaptee pour traiter la collision */
        MatPassage matriceBaseAdaptee(double x1, double y1,
                                      double x2, double y2) const;
        /* calcul des coordonnees polaires des vecteurs vitesse
         * dans cette base */
        VitessePolaire vitesseBaseAdaptee(MatPassage const& M,
                                    VitesseCartesienne const& vitInit) const;
        /* calcul de la vitesse apres le choc toujours dans cette base
         * (voir pdf qui contient les calculs) */
        VitessePolaire vitesseApresCol(VitessePolaire & oldVit,
                                        double m1, double m2) const;
        /* conversion de cette vitesse en coordonnees cartesiennes */
        VitesseCartesienne conversionCartesien(VitessePolaire const& newVit)
                                                                        const;
        /* conversion des coordonnees de vitFinale dans la base initiale
         * (matrice inverse de la matrice M) */
        void convBaseInitiale(VitesseCartesienne & vitFinale,
                                MatPassage const& M) const;

        /* renvoie le temps pour lequel il y a collision avec c2 pendant dt,
         * et -1 s'il n'y a pas collision */
        double testCollision(Corps const& c2);

        /* renvoie le temps pour lequel il y a collision avec c2 pendant dt,
         * et -1 s'il n'y a pas collision, en supposant le corps c immobile */
        double testCollision_immobile(Corps const & c2) const;

        /* si le corps doit cogner un autre corps pendant dt,
         * traite la collision en le faisant rebondir dessus
         * (on suppose l'autre immobile) */
        void rebondir(std::vector< double > const& tpsCol_immobile,
                        std::vector< Corps* > const& objets);

        /* actualise pos et renvoie true si le corps est dans l'ecran */
        bool inScreen(Couple const& decalageMap);

        /* s'occupe de l'affichage des corps et de l'interface */
        void affichage(sf::RenderWindow & window, Couple const& decalageMapX);

        double getX() const { return x; };
        double getY() const { return y; };
        double getVX() const;
        double getVY() const;
        double getM() const;
        unsigned int getId() const;

    private:

        sf::Color getColorFromHeat();
     
        /* le centre (0,0) est au milieu de l'ecran et la position de chaque
         * carre est celle de son centre */
        double x, y, vx, vy, ax, ay, r, m;
        double heat_;
        /* identifiant unique de l'objet */
        unsigned int id;
        /* image de l'objet */
        sf::CircleShape *img;
};

#endif // CORPS_H_INCLUDED

