#ifndef INTERFACE_H_INCLUDED
#define INTERFACE_H_INCLUDED

#include <SFML/Graphics.hpp>
#include "ConfInterface.h"
#include "Corps.h"
#include "InfosClavier.h"
#include "Simulation.h"
#include "Structures.h"
#include "Button.h"
#include "BodyCreator.h"

class Interface
{
    public:

        Interface(Simulation & simulation, sf::RenderWindow & window_,
                    sf::Font & font);

        /* traite les evenements quitter et pause et
         * enregistre les evenements clavier */
        void recordKeyboardEvents(sf::Event const& event);          
        void manageKeyboardEvents();
        void manageMouseEvents(sf::Event const& event);
        /* update the display */
        void display();

        /* affiche en bas a droite de l'ecran l'energie totale du systeme */
        void affichageNrjTotale(double nrj_totale);

        /* indique si la simulation est terminee */
        bool getDone() const;
        /* indique si la simulation est en pause */
        bool getPause() const;
        double getCurrentDelay() const { return currentDelay_; };

    private: 

        /* s'il y a eu appui sur les touches flechees,
         * deplace la vision de la map */
        void gestionDeplacementMap(); 
        /* s'il y a eu un evenement molette, actualise l'echelle la map */
        void gestionZoom();
        /* if R or T key pressed, change simulation speed (DT) */
        void speedManagement();
        /* affiche tous les objets de la simulation */
        void displaySimulation();
        /* display the text about the current speed of the simulation */
        void displaySpeed();


        InterfaceState state_;

        Simulation & simulation_;
        std::vector< Corps* >& objets;
        sf::RenderWindow& window;
        ConfInterface confInterface_;

        sf::Font font;
        sf::Text speedText_;
        /* text containing the string "MAX SPEED" */
        sf::Text fullSpeedText_;
        /* barres horizontale et verticale composant la croix au pt de
         * cordonnees (0,0) */
        sf::RectangleShape imgCroix1, imgCroix2;
        /* sert a afficher la croix au pt de coordonnees (0,0) */
        Couple posCroix;
        Button buttonRunning_;
        Button buttonPaused_;

        /* informations sur les touches du clavier */
        InfosClavier infosClavier;
        /* decalage de la vision de la map en X et Y.
         * si les deux = 0, alors le pt de coordonnees (0,0)
         * est au centre de l'ecran */
        Couple decalageMap;
        /* vaut true quand le programme se termine */
        bool done; 
        /* indique si la simulation est en pause */
        bool pause;
        /* indicates if the simulation was paused because the user went to edit
         * mode (this value has a meaning if pause == true) */
        bool pausedByEditing_;
        /* indicates if the speed text is up to date. if not, it should be
         * changed by the interface */
        bool speedTextUpToDate_;
        /* current delay between a computation of two iterations.
         * by default its equal to DT defined in parameters. */
        double currentDelay_;
};

#endif // INTERFACE_H_INCLUDED

