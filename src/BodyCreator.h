#ifndef BODYCREATOR_H_INCLUDED
#define BODYCREATOR_H_INCLUDED

#include "Simulation.h"

namespace BodyCreator {
    void init();
    void manageEvents(Simulation & simulation, sf::Event const& event,
                      Couple const& decalageMap, sf::RenderWindow & window);

    extern bool newBodyExists;
};

#endif //BODYCREATOR_H_INCLUDED

