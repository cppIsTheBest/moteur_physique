#include <iostream>
#include <fstream>
#include <cmath>
#include "Parameters.h"
#include "Corps.h"

#define arrondi(a) int(a + 0.5)

using namespace std;
using namespace sf;

double calculAngleVecteur(double x, double y);
double prodScalaire(double x1, double y1, double x2, double y2);
double prodScalaireCarre(double x, double y);
double solPolynome(double a, double b, double c, unsigned int min_max);



Corps::Corps(double x0, double y0, double vx0, double vy0, double r0,
             unsigned int id0)
{
    x = x0;     y = y0;
    vx = vx0;   vy = vy0;
    ax = ay = 0;
    r = r0;     m = pow(r, 2);
    heat_ = 0;
    id = id0;
    img = NULL;
}



Corps::~Corps()
{
    delete(img);
    img = NULL;
}



void Corps::create_img(double echelle)
{
    if(img == NULL) {
        img = new CircleShape(float(r*echelle));
        img->setFillColor(getColorFromHeat());
    }
}



void Corps::delete_img()
{
    delete(img);
    img = NULL;
}



void Corps::updatePosition()
{
    x += vx*Parameters::params["DT"];
    y += vy*Parameters::params["DT"];
}



void Corps::updateVitesse()
{
    vx += ax*Parameters::params["DT"];
    vy += ay*Parameters::params["DT"];
}



void Corps::modVitesse(Couple const& offset) {
    vx += offset.x;
    vy += offset.y;
}



void Corps::updateAcceleration(std::vector< Corps* > const& objets, double G)
{
    double  a(0), d(0), angle(0);

    ax = ay = 0;
    for(unsigned int i(0);i<objets.size();i++)
        if(objets[i]->id != id)
        {
            d = sqrt(pow(x-objets[i]->x, 2) + pow(y-objets[i]->y, 2));
            angle = calculAngleVecteur(objets[i]->x-x, objets[i]->y-y);
            a = (G*objets[i]->m)/pow(d, 2);
            ax += a*cos(angle);     ay += a*sin(angle);
        }
}



void Corps::gestionChoc(Corps & c, double t)
{
    /* on devrait placer les boules a l'emplacement de la collision
     * comme ci-dessous, mais je ne le fais pas car ca genere des problemes
     * pour lesquels je n'ai pas trouve de solution satisfaisante
        x += vx*t;          y += vy*t;
        c.x += c.vx*t;      c.y += c.vy*t;
    */

    VitesseCartesienne vitInit;
    vitInit.vx1 = vx;
    vitInit.vy1 = vy;
    vitInit.vx2 = c.vx;
    vitInit.vy2 = c.vy;

    // calcul de la base adaptee pour traiter la collision
    MatPassage M(matriceBaseAdaptee(x, y, c.x, c.y));

    // calcul des coordonnees polaires des vecteurs vitesse dans cette base
    VitessePolaire oldVit(vitesseBaseAdaptee(M, vitInit));

    // calcul de la vitesse apres le choc toujours dans cette base
    // (voir pdf qui contient les calculs)
    VitessePolaire newVit(vitesseApresCol(oldVit, m, c.m));

    // conversion de cette vitesse en coordonnees cartesiennes
    VitesseCartesienne vitFinale(conversionCartesien(newVit));

    // conversion de ces coordonnees dans la base initiale
    // (matrice inverse de la matrice M)
    convBaseInitiale(vitFinale, M);

    double f = sqrt(1-Parameters::params["NRJ_CONV_COLLISION"]);
    double v = sqrt(pow(vx, 2) + pow(vy, 2));
    double cv = sqrt(pow(c.vx, 2) + pow(c.vy, 2));
    double nrjKinColl = 0.50 * (m*pow(v, 2) + c.m*pow(cv, 2));

    vx = vitFinale.vx1 * f;
    vy = vitFinale.vy1 * f;
    c.vx = vitFinale.vx2 * f;
    c.vy = vitFinale.vy2 * f;

    // update heat of the two bodies
    heat_ += (f * nrjKinColl) / (Parameters::params["HEAT_CAPACITY"] * m);
    c.heat_ += (f * nrjKinColl) / (Parameters::params["HEAT_CAPACITY"] * c.m);

    // update colors of the two bodies
    if (img != nullptr)
        img->setFillColor(getColorFromHeat());
    if (c.img != nullptr)
        c.img->setFillColor(c.getColorFromHeat());
}



void Corps::gestionChoc_immobile(Corps const& c, double t)
{
    /* on devrait placer les boules a l'emplacement de la collision,
     * mais je ne le fais pas car ca genere des problemes
     * pour lesquels je n'ai pas trouve de solution satisfaisante
     * x += vx*t;
     * y += vy*t; */

    // calcul de la base adaptee pour traiter la collision
    MatPassage M(matriceBaseAdaptee(x, y, c.x, c.y));

    // calcul des composantes du vecteurs vitesse et de leurs angles
    // dans cette base
    double vx1 = M.x11*vx + M.x12*vy;
    double vy1 = M.x21*vx + M.x22*vy;
    double v1 = sqrt(vx1*vx1 + vy1*vy1);
    double a1 = calculAngleVecteur(vx1, vy1);

    // calcul de la vitesse apres le choc (voir pdf)
    double f((m-c.m)/(m+c.m));
    double new_a1 = atan(f*tan(a1));
    double new_v1 = sqrt(pow(f*v1*sin(a1),2)+pow(v1*cos(a1),2));
    /* correction de l'incertude sur l'angle causee par les arctan
     * dans la base adaptee a la collision, si l'angle du vecteur vitesse
     * etait a droite de la ligne d'impact, il y reste apres la collision */
    if ((a1 < 3.0*M_PI/2.0 && a1 > M_PI_2)
            || (a1 < -M_PI_2 && a1 > -3.0*M_PI/2.0))
        new_a1 += M_PI;

    // conversion de cette vitesse dans la base initiale
    // (matrice inverse de la matrice M)
    VitesseCartesienne vitFinale;
    vitFinale.vx1 = new_v1*cos(new_a1);
    vitFinale.vy1 = new_v1*sin(new_a1);
    vitFinale.vx2 = 0;
    vitFinale.vy2 = 0;
    convBaseInitiale(vitFinale, M);

    /* on ne change pas la vitesse du corps c, donc pour assurer la
     * conservation de l'energie cinetique, on s'assure que la norme de la
     * nouvelle vitesse du corps traite soit egale a la norme de son ancienne
     * vitesse (v1). */
    double new_norme( sqrt(vitFinale.vx1*vitFinale.vx1
                           + vitFinale.vy1*vitFinale.vy1) );
    vx = (vitFinale.vx1*v1)/new_norme;
    vy = (vitFinale.vy1*v1)/new_norme;
}



MatPassage Corps::matriceBaseAdaptee(double x1, double y1,
                                     double x2, double y2) const
{
    MatPassage M;

    double d;
    d = sqrt( (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) );

    //on cree le vecteur j de la base, colineaire a la ligne d'impact
    M.x12 = (x2-x1)/d;      M.x22 = (y2-y1)/d;
    double angle = calculAngleVecteur(M.x12, M.x22);   //on calcule son angle
    angle += M_PI_2;    //on a l'angle du vecteur i
    M.x11 = cos(angle);     M.x21 = sin(angle);     //coordonnees du vecteur i

    return M;
}



VitessePolaire Corps::vitesseBaseAdaptee(MatPassage const& M,
                                    VitesseCartesienne const& vitInit) const
{
    //les composantes des vitesses des deux corps dans la nouvelle base
    double f_vx1, f_vy1, f_vx2, f_vy2;
    //la vitesse dans la nouvelle base
    VitessePolaire vit;

    f_vx1 = M.x11*vitInit.vx1 + M.x12*vitInit.vy1;
    f_vy1 = M.x21*vitInit.vx1 + M.x22*vitInit.vy1;
    f_vx2 = M.x11*vitInit.vx2 + M.x12*vitInit.vy2;
    f_vy2 = M.x21*vitInit.vx2 + M.x22*vitInit.vy2;
    vit.v1 = sqrt(f_vx1*f_vx1 + f_vy1*f_vy1);
    vit.v2 = sqrt(f_vx2*f_vx2 + f_vy2*f_vy2);
    vit.a1 = calculAngleVecteur(f_vx1, f_vy1);
    vit.a2 = calculAngleVecteur(f_vx2, f_vy2);

    return vit;
}



VitessePolaire Corps::vitesseApresCol(VitessePolaire & oldVit, double m1,
                                                            double m2) const
{
    // la vitesse apres collision
    VitessePolaire vit;
    double f = (m1-m2)/(m1+m2);

    if(oldVit.v1 == 0.0 && oldVit.v2 == 0.0)
    {
        // si les vitesses sont nulles (bizarre mais bon on sait jamais)
        // les vitesses finales sont nulles aussi
        vit.v1 = 0.0;
        vit.v2 = 0.0;
        // on donne une valeur sans signification a a1f et a2f
        vit.a1 = 1.0;
        vit.a2 = 1.0;
    }
    else if(cos(oldVit.a1) == 0.0 && cos(oldVit.a2) == 0.0)
    {
        // situation equivalente au probleme en dimension 1
        vit.v1 = f*oldVit.v1 + 2.0*m2*(m1+m2)*oldVit.v2;
        vit.v2 = 2.0*m1*(m1+m2)*oldVit.v1 + (-f)*oldVit.v2;
        vit.a1 = -oldVit.a1;
        vit.a2 = -oldVit.a2;
    }
    else
    {
        /* desole c'est ULTRA-MOCHE mais c'est trop complique d'eviter toutes
         * les divisions par 0 (et de toute facon avoir une vitesse exactement
         * nulle est hautement improbable) */
        if(oldVit.v1 == 0.0)
            oldVit.v1 = 0.0000001;
        if(oldVit.v2 == 0.0)
            oldVit.v2 = 0.0000001;
        if(cos(oldVit.a1) == 0.0)
            oldVit.a1 += oldVit.a1/1000000;
        if(cos(oldVit.a2) == 0.0)
            oldVit.a2 += oldVit.a2/1000000;

        // systeme d'equations (23) du pdf
        vit.a1 = atan( tan(oldVit.a1)*f
                + (2.0*m2/(m1+m2))*(oldVit.v2/oldVit.v1)*(sin(oldVit.a2)
                    /cos(oldVit.a1)) );
        double z = oldVit.v1*sin(oldVit.a1)*f
                + oldVit.v2*sin(oldVit.a2)*2.0*m2/(m1+m2);
        vit.v1 = sqrt( z*z
                + oldVit.v1*cos(oldVit.a1)*oldVit.v1*cos(oldVit.a1) );
        vit.a2 = atan( tan(oldVit.a2)*(-f)
                + 2.0*(m1/(m1+m2))*(oldVit.v1/oldVit.v2)*(sin(oldVit.a1)
                    /cos(oldVit.a2)) );
        z = oldVit.v2*sin(oldVit.a2)*(-f) +
                oldVit.v1*sin(oldVit.a1)*2.0*m1/(m1+m2);
        vit.v2 = sqrt( z*z
                + oldVit.v2*cos(oldVit.a2)*oldVit.v2*cos(oldVit.a2) );

        /* correction de l'incertude sur l'angle causee par les arctan
         * dans la base adaptee a la collision, si l'angle du vecteur vitesse
         * etait a droite de la ligne d'impact, il y reste apres la collision */
        if ((oldVit.a1 < 3.0*M_PI/2.0 && oldVit.a1 > M_PI_2)
             || (oldVit.a1 < -M_PI_2 && oldVit.a1 > -3.0*M_PI/2.0))
                vit.a1 += M_PI;
        if ((oldVit.a2 < 3.0*M_PI/2.0 && oldVit.a2 > M_PI_2)
             || (oldVit.a2 < -M_PI_2 && oldVit.a2 > -3.0*M_PI/2.0))
                vit.a2 += M_PI;

    }

    return vit;
}



VitesseCartesienne Corps::conversionCartesien(VitessePolaire const& newVit)
                                                                        const
{
    VitesseCartesienne vitCart;

    vitCart.vx1 = newVit.v1*cos(newVit.a1);
    vitCart.vy1 = newVit.v1*sin(newVit.a1);
    vitCart.vx2 = newVit.v2*cos(newVit.a2);
    vitCart.vy2 = newVit.v2*sin(newVit.a2);

    return vitCart;
}



void Corps::convBaseInitiale(VitesseCartesienne & vitFinale,
                                                    MatPassage const& M) const
{
    /* on repasse maintenant dans la base initale a l'aide de la matrice inverse
     * de M (1/det M) * | x22 -x12|  =   | A11 A12 |                                                    |-x21 x11 |      | A21 A22 |
    */
    double det(M.x11*M.x22 - M.x21*M.x12);
    double A11(M.x22/det), A12(-M.x12/det), A21(-M.x21/det), A22(M.x11/det);

    //les composantes finales sont :
    double f_vx1(vitFinale.vx1);
    vitFinale.vx1 = A11*vitFinale.vx1 + A12*vitFinale.vy1;
    vitFinale.vy1 = A21*f_vx1 + A22*vitFinale.vy1;

    double f_vx2(vitFinale.vx2);
    vitFinale.vx2 = A11*vitFinale.vx2 + A12*vitFinale.vy2;
    vitFinale.vy2 = A21*f_vx2 + A22*vitFinale.vy2;
}



double Corps::testCollision(Corps const& c2)
{
    double t(-1.0), a, b, c;

    if(id != c2.id)
    {
        a = prodScalaireCarre(vx-c2.vx, vy-c2.vy);
        b = 2*prodScalaire(vx-c2.vx, vy-c2.vy, x-c2.x, y-c2.y);
        c = prodScalaireCarre(x-c2.x, y-c2.y) - (r+c2.r)*(r+c2.r);
        t = solPolynome(a, b, c, 1);

        if(t > Parameters::params["DT"] || t <= 0)
            t = -1.0;
    }

    return t;
}



double Corps::testCollision_immobile(Corps const& c2) const
{
    double t(-1.0), a, b, c;

    if(id != c2.id)
    {
        a = prodScalaireCarre(vx, vy);
        b = 2*prodScalaire(vx, vy, x-c2.x, y-c2.y);
        c = prodScalaireCarre(x-c2.x, y-c2.y) - (r+c2.r)*(r+c2.r);
        t = solPolynome(a, b, c, 1);

        if(t > Parameters::params["DT"] || t <= 0)
            t = -1.0;
        else
            return t;
    }

    return t;
}



void Corps::rebondir(vector< double > const& tpsCol_immobile,
                     vector< Corps* > const& objets)
{
    //on trouve la collision qui aura lieu en premier
    double tps_min(Parameters::params["DT"]+1);
    unsigned int i_min(0);

    for(unsigned int i(0);i<tpsCol_immobile.size();i++)
        if(tpsCol_immobile[i] < tps_min && tpsCol_immobile[i] > 0)
        {
            tps_min = tpsCol_immobile[i];
            i_min = i;
        }

    //si le corps entre en collision avec un autre, on traite le rebond
    if(tps_min > 0 && tps_min <= Parameters::params["DT"])
        gestionChoc_immobile(*objets[i_min], tps_min);
    //sinon on se contente de mettre a jour sa position normalement
    else
        updatePosition();
}



bool Corps::inScreen(Couple const& decalageMap)
{
    float pos_img_x = x*Parameters::params["ECHELLE"]
            + Parameters::params["T_FENETRE_X"]/2
            - r*Parameters::params["ECHELLE"] + decalageMap.x;
    float pos_img_y = y*Parameters::params["ECHELLE"]
            + Parameters::params["T_FENETRE_Y"]/2
            - r*Parameters::params["ECHELLE"] + decalageMap.y;

    return pos_img_x + arrondi(2*r*Parameters::params["ECHELLE"]) >= 0
            && pos_img_y + arrondi(2*r*Parameters::params["ECHELLE"]) >= 0
            && pos_img_x < arrondi(Parameters::params["T_FENETRE_X"])
            && pos_img_y < arrondi(Parameters::params["T_FENETRE_Y"]) ;
}



void Corps::affichage(RenderWindow & window, Couple const& decalageMap)
{
    float pos_img_x = x*Parameters::params["ECHELLE"]
                      + Parameters::params["T_FENETRE_X"]/2
                      - r*Parameters::params["ECHELLE"]
                      + decalageMap.x;
    float pos_img_y = y*Parameters::params["ECHELLE"]
                      + Parameters::params["T_FENETRE_Y"]/2
                      - r*Parameters::params["ECHELLE"]
                      + decalageMap.y;

    //std::cout << "affichage " << id << " | " << img << std::endl;
    img->setPosition(pos_img_x, pos_img_y); 
    window.draw(*img);
}



double Corps::getVX() const {
    return vx;
}



double Corps::getVY() const {
    return vy;
}



double Corps::getM() const {
    return m;
}



unsigned int Corps::getId() const
{
    return id;
}

sf::Color Corps::getColorFromHeat() {
    static double heatMax = 50000;
    if (heat_ < heatMax/3) {
        return Color(64 + (255-64)*heat_/(heatMax/3), 64, 64);
    } else if (heat_ < 2*heatMax/3) {
        return Color(255, 64 + (255-64)*(heat_-heatMax/3)/(heatMax/3), 64);
    } else if (heat_ < heatMax) {
        return Color(255, 255, 64 + (255-64)*(heat_-2*heatMax/3)/(heatMax/3));
    } else {
        return Color(255, 255, 255);
    }
}

