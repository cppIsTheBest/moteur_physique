#ifndef BUTTON_H
#define BUTTON_H

#include <SFML/Graphics.hpp>
#include <string>
#include "Structures.h"

class Button
{
    public:
        Button();
        Button(sf::Font & font, std::string text, int sizePixelText,
                sf::Color frColor, sf::Color bgColor);
        Button(Button const& button);

        void init(sf::Font & font, std::string text, int sizePixelText,
                sf::Color frColor, sf::Color bgColor);
        /* display the button */
        void display(sf::RenderWindow & window);
        void setPosition(int x, int y);
        /* set the string of the m_text attribute */
        /* set the string of the m_text attribute */
        void setString(std::string str);

        /* return true if the event is a click and if the click was in the
         * button */
        bool isClicked(sf::Event const& event) const;
        /* return the size of the background */
        Couple getSizeBG() const;
        /* return the size of the m_text attribute */
        Couple getSizeText() const;

        /* accessors */
        sf::Font const& getFont() const { return *m_font; };
        sf::Text const& getText() const { return m_text; };
        std::string getString() const {
            return m_text.getString().toAnsiString(); };
        sf::RectangleShape const& getBackgroundShape() const {
            return m_backgroundShape; };
        int getBackgroundMargin() const { return m_backgroundMargin; };
        bool getAlarmState() const { return m_alarmState; };
        sf::FloatRect getGlobalBounds() const {
            return m_backgroundShape.getGlobalBounds(); };

    private:
        /* reset the size of the background of the text to correspond to the
         * m_text attribute */
        void setSizeBackground();

        /* the font of the button (by default "DejaVuSans.ttf") */
        sf::Font* m_font;
        /* text of the button */
        sf::Text m_text;
        /* uniform color of the background of the text */
        sf::RectangleShape m_backgroundShape;
        /* margin between text and background, on each four directions */
        int m_backgroundMargin;
        /* indicates if the button is in alarm state */
        bool m_alarmState;
};

#endif // BUTTON_H

